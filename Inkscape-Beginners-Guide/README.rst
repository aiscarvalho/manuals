*************************
Inkscape Beginners' Guide
*************************

This is the collaborative area for the Inkscape Beginners' Guide, an introductory practical manual for people who are new to Inkscape.

It is based on the book '`Initiation à Inkscape <https://fr.flossmanuals.net/initiation-inkscape/>`_' by Elisa de Castro Guerra.

The book's contents is licensed CC BY-SA.

Installing the required software
================================

Option 1: Scripted installation (Linux only)
--------------------------------------------

Requires Python 3, make and git to be installed and your SSH key to be set up with GitLab.

Download `the setup script <https://gitlab.com/inkscape/inkscape-docs/manuals/-/raw/master/Inkscape-Beginners-Guide/utils/setup.sh?inline=false>`_ and save it in the directory which you want to contain the repository.

Run the script with:

.. code-block:: none

  sh setup.sh

This will clone this repository, set up the Python environment and install the necessary Python packages and build a first set of the documentation for you.

Option 2: Manual installation
-----------------------------

#. Inside a folder of your choice, clone this repository:

   Via HTTPS:

   .. code-block:: none

     git clone https://gitlab.com/inkscape/inkscape-docs/manuals.git

   If your SSH key is setup with GitLab, you can also use the following instead:

   .. code-block:: none

     git clone git@gitlab.com:inkscape/inkscape-docs/manuals.git

#. Then, enter into the Beginners' Guide directory:

   .. code-block:: none

     cd manuals/Inkscape-Beginners-Guide/

#. And create a virtual Python3 environment for your work:

   .. code-block:: none

     python3 -m venv venv

#. Activate the environment:

   On a Linux shell (scroll down to the table below to see which command you need on your system):

   .. code-block:: none

     source venv/bin/activate

#. Now install the required Python packages:

   .. code-block:: none

     pip install wheel
     pip install sphinx latex sphinx-rtd-theme

If you need to deactivate the Python environment, you can do this (on Linux) with:

.. code-block:: none

  deactivate

Building the Guide
==================

First, activate your virtual Python environment using one of these commands:

========== ===================================
Shell      Command to activate the environment
========== ===================================
bash/zsh   source venv/bin/activate
fish       venv/bin/activate.fish
csh/tcsh   source venv/bin/activate.csh
cmd.exe    venv\\Scripts\\activate.bat
PowerShell venv\\Scripts\\Activate.ps1
========== ===================================

To build a set of html files (generated index file is ``build/html/index.html``), run:

.. code-block:: none

  make html

To build a pdf file (generated file is ``build/latex/InkscapeBeginnersGuide.pdf``), run:

.. code-block:: none

  make latexpdf

To remove the build directory before building, run

.. code-block:: none

  make clean

This can become necessary when you make changes in one file that also affect other html files, e.g. changing a title (that will change in the menu), adding a chapter (that will appear in the menu), or when you change static files (css, js, images), as Sphinx seems to only pick up changes in .rst files.


Branch structure
================

- master: The master branch is the default branch at `Read the docs <https://inkscape-manuals.readthedocs.io/>`_ (as 'latest').
- unstable: This is the branch `used for testing on Read the docs <https://inkscape-manuals.readthedocs.io/en/unstable/>`_.

TODO
====

Content:

- rethink introductory chapters (titles sound wrong, need an explanation about what Inkscape is)
- consider adding intro chapters to each section (some have one)
- write / fix anything else that is marked with TODO in the source folder
- markup useful terms, add them to glossary and write definitions
- decide on 'we <are going to>...' vs. 'you <are going to>...' and make it consistent throughout
- add more subheadings

Images:

- change out all icons for the actual Inkscape icon SVGs
- create missing, untranslated screenshots
- find out if it is at all possible to influence image sizes in pdf export, via some kind of 'classes'
- when all is ready, remove unused images (not icons)

General layout:

- setup a generalized chapter head and make it look nice:

  * heading
  * icon / keyboard shortcut if applicable
  * how to get there via menu, if applicable
- find out how to add at least some minimal styling to pdf
- remove the old fonts / css directory after it was incorporated into pdf styling
- use non-breaking spaces in keyboard shortcut combinations
- make guilabel background color darker

Infrastructure:

- close book draft on flossmanuals, add info about new home
- ? Add CI to test for syntax errors ? into the css / pdf builder
- Add license for Inkscape GUI icons into icons directory

Super Bonus Extras:

- test js. If it works, then:

  * explore js to load glossary term definitions from the glossary page (same domain, good!), and show them on hover in a text bubble (or find someone who would like to write that)
  * explore js that scrolls menu to correct place (or find someone who would like to write that). It needs to scroll to an element in the navigation that has both the class 'current' and whose link is that of the current page, when the page is loaded (but preferably, it should not scroll visibly, but before the navigation appears). Must work both on mobile and desktop.
