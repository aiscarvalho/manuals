*********************************
Ordering Markers, Stroke and Fill
*********************************

An object's path determines its shape. The :term:`stroke <Stroke>` is centered on
the path, one half of it on the inside of the path, overlapping with the
fill, the other half is on the outside. Markers, too, are centered on the
path.

In the :guilabel:`Order` section of the :guilabel:`Fill and Stroke` dialog, you
can select the order in which these different parts of an object will be drawn.
This way, you can place markers (and / or strokes) above or below the fill.

.. figure:: images/ordre-marqueurs.png
    :alt: Marker order icons
    :class: screenshot

    The available marker orders’ icons

.. figure:: images/order_markers_example_1.png
    :alt: Marker order: markers > stroke > fill
    :class: screenshot

    [top] markers > stroke > fill [bottom]


.. TODO: Example image needed!

.. figure:: images/order_markers_example_1.png
    :alt: Marker order: markers > stroke > fill
    :class: screenshot

    [top] markers > stroke > fill [bottom]

.. figure:: images/order_markers_example_2.png
    :alt: Marker order: markers > fill > stroke
    :class: screenshot

    [top] markers > fill > stroke [bottom]

.. figure:: images/order_markers_example_3.png
    :alt: Marker order: stroke > marker > fill
    :class: screenshot

    [top] stroke > marker > fill [bottom]

.. figure:: images/order_markers_example_4.png
    :alt: Marker order: stroke > fill > markers
    :class: screenshot

    [top] stroke > fill > markers [bottom]

.. figure:: images/order_markers_example_5.png
    :alt: Marker order: fill > markers > stroke
    :class: screenshot

    [top] fill > markers > stroke [bottom]

.. figure:: images/order_markers_example_6.png
    :alt: Marker order: stroke > fill > marker
    :class: screenshot

    [top] stroke > fill > marker [bottom]
