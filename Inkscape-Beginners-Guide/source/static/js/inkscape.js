var glossary_entries = {};
$(document).ready(function()
{
  $.ajax({
  url: 'glossary.html',
  success: getGlossaryData,
  dataType: 'html',
  timeout: 5000,
  mimeType: "text/html; charset=utf-8",})
});

function getGlossaryData(data, textStatus, jqXHR)
{
  var result = $(data).find('dl').first()
  result.children("dt").each(function(){
    glossary_entries[$(this).attr('id')] = $(this).next('dd').html();
  });
  $("span.std-term").each(function(){
    var glossary_link = $(this).parent();
    var glossary_anchor = glossary_link.attr('href').split('#').pop();
    var description = glossary_entries[glossary_anchor];
    var tt_div = $('<div/>', {class:'tooltip_content'});
    tt_div.html(description);
    $(glossary_link).append(tt_div);
  })
}
